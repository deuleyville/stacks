from flask import Flask
app = Flask(__name__)

@app.route('/')
def hello_world():
    return 'Hello, World!'

@app.route('/stack/<id>')
def stack_get_or_generate(id):
    # if generated, retrieves stack
    # if not generated, creates a stack based on that string, saves it, returns result
    return "there's a stack for that, it's " + str(id) + "!"

@app.route('/stack/<id>/regenerate')
def regenerate_stack(id):
    # if not claimed, creates new stack with string, overwrites, returns result
    # if claimed, returns 403
    return "regenerating stack!"

@app.route('/stack/<id>/save')
def claim_stack(id):
    # if not claimed...
    #     checks uniqueness of handle
    #     checks twitter API for handle
    #     saves record
    # if claimed, returns 403
    return "stack claimed!"

@app.route('/stack/<id>/edit')
def edit_stack(id):
    # looks for optional params
    # ?token=<token>?tech=[id,id]?quote=<str>,inventor=<str>,invented_on=<timestamp>
    # auth required - overwrites stack with provided values
    # all fields optional, only overwrite fields provided
    return "stack edited!"

@app.route('/stack/<id>/delete')
def delete_stack(id):
    # auth required - deletes saved stack
    return "stack deleted!"

@app.route('/tech/create')
def create_tech():
    # creates tech stack
    # no auth provided - saves it with hidden=True
    # auth provided - saves it with no hidden flag
    return "tech created!"

@app.route('/tech/<id>/delete')
def delete_tech(id):
    # auth required
    # pulls list of stacks with existing dependency
    # finds alternate tech to replace with
    # iterates through existing, replaces with new tech, saves
    # deletes tech
    return "tech deleted!"